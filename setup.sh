#!/bin/sh

BLUE='\033[1;34m'
YELLOW='\033[1;33m'
NC='\033[0m'

# Check for root
if [[ $EUID -ne 0 ]]; then
     echo
     echo "[!] This script must be ran as root."
     exit
fi

if [ -d /pentest ]; then
    echo -e "${BLUE}Updating Discover.${NC}"
    git pull
    echo
    echo
    exit
else 
    if [ -d /opt/discover/ ]; then
         echo -e "${YELLOW}Installing Discover.${NC}"
         cd /opt/discover/
         sudo ./update.sh
    else
         cd /opt/
         git clone https://github.com/leebaird/discover.git
         cd discover/
         ./update.sh
    fi
fi

if [ -d /opt/SecLists/ ]; then
     echo -e "${YELLOW}SecLists Already Present.${NC}"
else
     echo "${YELLO}Installing SecLists.${NC}"
     cd /opt/
     git clone https://github.com/danielmiessler/SecLists.git
fi

if [ ! -d ~/Downloads/Burpsuit.zip ]; then
     echo "${YELLOW}Installing Burpsuit.${NC}"
     cd /opt/
     mkdir Burpsuite
     cd Burpsuite
     git clone git@bitbucket.org:kartikagrawal530/burpsuite.git
     apt-get install unzip
     cd burpsuite
     unzip Burpsuite.zip
     cd Burpsuite
     wget https://builds.openlogic.com/downloadJDK/openlogic-openjdk/8u262-b10/openlogic-openjdk-8u262-b10-linux-x64-deb.deb
     dpkg -i openlogic-openjdk-8u262-b10-linux-x64-deb.deb
     update-alternatives --config java
     java -noverify -jar ESEdition.jar
else 
     echo "${YELLOW}Burpsuite Already installed.${NC}"
fi
exit

